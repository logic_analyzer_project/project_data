import serial
import time

import threading
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import time
import numpy as np

ser = serial.Serial('COM4', 115200, timeout=1)
time.sleep(2)

# global
y = []


def getbinary(x, n): return format(x, 'b').zfill(n)


def reading():
    global ser
    while(True):
        line = ser.readline()   # read a byte string
        if line:
            if line == b'S\r\n':
                print("initial message")
            else:
                s = line.decode()
                if not s == "":
                    bindata = ''
                    lst = []
                    localy = []
                    for ss in s:
                        if ss.isdigit():
                            lst.append(int(ss))
                    for d in lst:
                        bindata += getbinary(d, 4)
                    for sss in bindata:
                        localy.append(int(sss))
                    global y
                    y = localy
                    print(bindata)


t1 = threading.Thread(target=reading)

t1.start()


# plotting

fig, ax = plt.subplots()
x, y = [], [1, 0, 1, 1, 0]
ln, = plt.step([], [])


def init():
    ax.set_xlim(0, 20)
    ax.set_ylim(0, 1)
    return ln,


def update(frame):
    global y
    x = np.arange(len(y)+1)
    my = [y[0]] + y
    ln.set_data(x, my)
    return ln,


ani = FuncAnimation(fig, update, frames=np.linspace(
    0, 2*np.pi, 128), init_func=init, blit=True)

plt.show()
