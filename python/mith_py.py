import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

import serial
import time

ser = serial.Serial('COM4', 115200, timeout=1)
time.sleep(2)


fig, ax = plt.subplots()
x, y = [], []

ln, = plt.step([], [])
def getbinary(x, n): return format(x, 'b').zfill(n)


def init():
    ax.set_xlim(0, 20)
    ax.set_ylim(0, 5)
    return ln,


def update(frame):
    ln.set_data(x, y)
    return ln,


ani = FuncAnimation(fig, update, frames=np.linspace(
    0, 2*np.pi, 128), init_func=init, blit=True)

# plt.step(x,y)
# plt.ylim(100,100);
# plt.show()

for i in range(5000):
    line = ser.readline()   # read a byte string
    if line:
        # print (line)
        if line == b'S\r\n':
            pass
        else:
            s = line.decode()  # convert the byte string to a unicode string
            bindata = ''
            lst = []
            for ss in s:
                try:
                    bindata += getbinary(int(ss), 4)
                    for b in bindata:
                        lst.append(b)
                    print(bindata)
                    print(y)
                    y = lst
                    print(y)
                except:
                    pass


ser.close()